# Browser Profiles

## What is it

Allows you manage isolated browser profiles. For example you may have 'work' and 'home' profiles with separated configurations, plugins, history, cache, etc.

Currently only supports Firefox.

## Usage

```bash
  ff -c work   # create a profile named "work" and open the browser with it
  ff /some/dir # open the browser with the profile stored in /some/dir
  ff -l        # show all profiles
  ff -x        # create a temporary profile and open the browser with it
  ff -h        # show more options
```

## Installation

Requires firefox and bash. (Currently only supports firefox browser).

### Installation script

Run 
`./install`

### Manual installation:

* Copy ff to your PATH.
* Copy or write a config file located at the global or local destination (see the first lines of ff).
* Check the `LANG_HOME` variable in your configuration points at the lang directory.
* Optionally create a home directory for shared profiles. Set it up in the config and give it rwX permissions for all users.


## Configuration

The configuration is taken from a global config file in /etc (see the exact path at the first lines of ff) and a per-user configuration. Per-user definitions have precedence over global definitions.

The sample config file is self documented.

